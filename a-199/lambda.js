let AWS = require('aws-sdk');
const kinesis = new AWS.Kinesis();

exports.handler = function (event, context, callback) {
    kinesis.describeStream({
        StreamName: 'k'
    }).promise()
        .then(data => {
            // your logic goes heres
        })
        .catch(err => {
            // error handling goes here
        });

    callback(null, { "message": "Successfully executed" });
}